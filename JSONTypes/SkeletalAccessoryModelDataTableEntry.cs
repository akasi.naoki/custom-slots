using Newtonsoft.Json.Linq;

namespace CustomSlots.JTypes
{
    class SkeletalAccessoryModelDataTableEntry
    {
        public string Name;
        public string MeshData;
        public string TargetSkeleton;
        public bool bCastShadow;
        private string ReplacementMaterianName1;
        private string ReplacementMaterianName2;
        private string ReplacementMaterianName3;
        private string ReplacementMaterianName4;
        private string ReplacementMaterianName5;
        private string ReplacementMaterianName6;
        public SkeletalAccessoryModelDataTableEntry(string inName, string inMeshData, string inTargetSkeleton, bool inbCastShadow)
        {
            Name = inName+"(0)";
            MeshData = "/Game/Model/Character/"+inMeshData+"(0)";
            TargetSkeleton = inTargetSkeleton; // EAttachAccessorySkeleton::BODY(0) or EAttachAccessorySkeleton::HAIR(0)
            bCastShadow = inbCastShadow;
            // not used for now
            ReplacementMaterianName1 = null;
            ReplacementMaterianName2 = null;
            ReplacementMaterianName3 = null;
            ReplacementMaterianName4 = null;
            ReplacementMaterianName5 = null;
            ReplacementMaterianName6 = null;
        }

        public JObject ToJson() 
        {
            var templ = JObject.Parse(jsonTemplates.SkeletalAccessoryModelDataTableItem);
            templ["Name"] = Name;
            templ["Value"][0]["Value"] = MeshData;
            templ["Value"][1]["Value"] = TargetSkeleton;
            templ["Value"][2]["Value"] = bCastShadow;

            templ["Value"][3]["Value"] = ReplacementMaterianName1;
            templ["Value"][4]["Value"] = ReplacementMaterianName2;
            templ["Value"][5]["Value"] = ReplacementMaterianName3;
            templ["Value"][6]["Value"] = ReplacementMaterianName4;
            templ["Value"][7]["Value"] = ReplacementMaterianName5;
            templ["Value"][8]["Value"] = ReplacementMaterianName6;

            return templ;
        }
    }
}