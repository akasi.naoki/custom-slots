using Newtonsoft.Json.Linq;

namespace CustomSlots.JTypes
{
    class MD_DLCUnlockConditionSteamEntry
    {
        public string Name;
        private int AppID;
        public MD_DLCUnlockConditionSteamEntry(string inName)
        {
            Name = inName+"(0)";
            AppID = 1738180; // Fixed
        }

        public JObject ToJson() 
        {
            var templ = JObject.Parse(jsonTemplates.MD_DLCUnlockConditionSteamItem);
            templ["Name"] = Name;
            templ["Value"][0]["Value"] = AppID;

            return templ;
        }
    }
}