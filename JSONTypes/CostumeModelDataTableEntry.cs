using Newtonsoft.Json.Linq;

namespace CustomSlots.JTypes
{
    class CostumeModelDataTableEntry
    {
        public string Name;
        public string BodyMesh;
        public string BodyGmPhysicsAsset;
        public string WearMesh;
        private string TextureData1;
        private string TargetTextureName1;
        private string TextureData2;
        private string TargetTextureName2;
        private string TextureData3;
        private string TargetTextureName3;
        private string AccessoryReplacementMaterialInfo;
        private string ParticleData;
        private int GimmickChannel;
        private float Duration; // defaults to '+0' for some reason
        private bool bIsTrail;
        private string AttachSkeleton;
        private string AttachBone1_2; // AttachBone1(2)
        private string AttachBone1_3; // AttachBone1(3)
        private float Rot1_X; // defaults to '+0' for some reason
        private float Rot1_Y; // defaults to '+0' for some reason
        private float Rot1_Z; // defaults to '+0' for some reason
        private string AttachBone2_2; // AttachBone2(2)
        private string AttachBone2_3; // AttachBone2(2)
        private float Rot2_X; // defaults to '+0' for some reason
        private float Rot2_Y; // defaults to '+0' for some reason
        private float Rot2_Z; // defaults to '+0' for some reason
        private float WidthScale; // defaults to '+0' for some reason
        private string HideBodyBone1;
        private string HideBodyBone2;
        private bool bNeedToonSubsurfacePostProcess;

        public CostumeModelDataTableEntry(string inName, string inBodyMesh, string inBodyGmPhysicsAsset)
        {
            Name = inName+"(0)";
            BodyMesh = "/Game/Model/Character/Body/Cos/"+inBodyMesh;
            BodyGmPhysicsAsset = inBodyGmPhysicsAsset;
            WearMesh = "None(0)";
            // not used for now
            TextureData1 = "None(0)";
            TargetTextureName1 = null;
            TextureData2 = "None(0)";
            TargetTextureName2 = null;
            TextureData3 = "None(0)";
            TargetTextureName3 = null;
            AccessoryReplacementMaterialInfo = null;
            ParticleData = "None(0)";
            GimmickChannel = 0;
            Duration = 0;
            bIsTrail = false;
            AttachSkeleton = "EAttachAccessorySkeleton::FACE(0)";
            AttachBone1_2 = "None(0)";
            AttachBone1_3 = "None(0)";
            Rot1_X = 0;
            Rot1_Y = 0;
            Rot1_Z = 0;
            AttachBone2_2 = "None(0)";
            AttachBone2_3 = "None(0)";
            Rot2_X = 0;
            Rot2_Y = 0;
            Rot2_Z = 0;
            WidthScale = 0;
            HideBodyBone1 = "None(0)";
            HideBodyBone2 = "None(0)";
            bNeedToonSubsurfacePostProcess = false;
        }

        public JObject ToJson() 
        {
            var templ = JObject.Parse(jsonTemplates.CostumeModelDataTableItem);
            templ["Name"] = Name;
            templ["Value"][0]["Value"] = BodyMesh;
            templ["Value"][1]["Value"] = BodyGmPhysicsAsset;

            templ["Value"][2]["Value"] = WearMesh;
            templ["Value"][3]["Value"][0]["Value"] = TextureData1;
            templ["Value"][3]["Value"][1]["Value"] = TargetTextureName1;
            templ["Value"][4]["Value"][0]["Value"] = TextureData2;
            templ["Value"][4]["Value"][1]["Value"] = TargetTextureName2;
            templ["Value"][5]["Value"][0]["Value"] = TextureData3;
            templ["Value"][5]["Value"][1]["Value"] = TargetTextureName3;
            templ["Value"][6]["Value"] = AccessoryReplacementMaterialInfo;
            templ["Value"][7]["Value"][0]["Value"] = ParticleData;
            templ["Value"][7]["Value"][1]["Value"] = GimmickChannel;
            templ["Value"][7]["Value"][2]["Value"] = Duration;
            templ["Value"][7]["Value"][3]["Value"] = bIsTrail;
            templ["Value"][7]["Value"][4]["Value"] = AttachSkeleton;
            templ["Value"][7]["Value"][5]["Value"] = AttachBone1_2;
            templ["Value"][7]["Value"][6]["Value"] = AttachBone1_3;
            templ["Value"][7]["Value"][7]["Value"] = Rot1_X;
            templ["Value"][7]["Value"][8]["Value"] = Rot1_Y;
            templ["Value"][7]["Value"][9]["Value"] = Rot1_Z;
            templ["Value"][7]["Value"][10]["Value"] = AttachBone2_2;
            templ["Value"][7]["Value"][11]["Value"] = AttachBone2_3;
            templ["Value"][7]["Value"][12]["Value"] = Rot2_X;
            templ["Value"][7]["Value"][13]["Value"] = Rot2_Y;
            templ["Value"][7]["Value"][14]["Value"] = Rot2_Z;
            templ["Value"][7]["Value"][16]["Value"] = WidthScale;
            templ["Value"][8]["Value"] = HideBodyBone1;
            templ["Value"][9]["Value"] = HideBodyBone2;
            templ["Value"][10]["Value"] = bNeedToonSubsurfacePostProcess;

            return templ;
        }
    }
}