﻿using System.Threading;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using CustomSlots.JTypes;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;	
namespace CustomSlots
{
    public static class CustomSlots
    {
        public static void updateOutput(string text) 
        {
            Gtk.Application.Invoke (delegate {
                Program.win.setOutputText(text);
            });
        }
        private static List<Costume> ReadCostumeJson(string costumePaks)
        {
            List<Costume> costumeList = new List<Costume>();
            var allPakFiles = Directory.GetFiles(costumePaks, "*.pak", SearchOption.TopDirectoryOnly);

            foreach (var f in allPakFiles)
            {
                var pakFileName = Path.GetFileName(f);
                var jsonFile = Path.Combine(costumePaks, Path.GetFileNameWithoutExtension(f)+".json");
                if (File.Exists(jsonFile))
                {
                    using (StreamReader file = File.OpenText(jsonFile))
                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        JObject jobj = (JObject) JToken.ReadFrom(reader);
                        costumeList.Add(new Costume(pakFileName, jobj["name"].ToString(), jobj["description"].ToString()));
                        //Extra Stuff I want to add
                    
                    
                    }
                } else
                {
                    updateOutput($"No json found for {pakFileName}");
                    costumeList.Add(new Costume(pakFileName, Path.GetFileNameWithoutExtension(f), ""));
                }
            }

            return costumeList;
        }

        public static void Rebuild(string gamePaks)
        {
            var currentPath = AppContext.BaseDirectory;
            var costumePaks = Path.Combine(currentPath, "costumes");
            var extractedFolder = Path.Combine(currentPath, "extracted");

            List<Costume> costumeList = ReadCostumeJson(costumePaks);
            //Used to store new custom Accessories to be added to the datatable
            //List<AttachAccessory> accessoryList = new List<AttachAccessory>();
            var gamePacksCheck = Directory.GetFiles(gamePaks, "*.pak", SearchOption.TopDirectoryOnly);
            if (gamePacksCheck.Length == 0)
            {
                updateOutput("No .pak files found, make sure to select the /Paks/ folder inside the game folder.");
                return;
            }

            if (costumeList.Count == 0)
            {
                updateOutput($"No mods found, make sure to copy the pak files with their respective json files in {costumePaks}");
                return;
            }

            var initID = "c";
            var initSort = 2000;
            for (int i = 0; i < costumeList.Count; i++)
            {
                costumeList[i].ID = initID+i.ToString("d5");
                costumeList[i].SortNum = initSort+i;
            }

            updateOutput("Extracting datatables from game paks...");
            Directory.CreateDirectory(extractedFolder);
            PakUtils.ExtractFromPak(gamePaks, PakFiles.GetAllFiles(), extractedFolder);
            // Costumes with mesh variations (not sure if missing some)
            string[] CostumeIDsWithVariation = {"cos001A","cos005A","cos006A"}; // cos002 , cos109 not supported yet
            string[] CostumeIDsWithVariationNoLetter = {"cos001","cos005","cos006"}; //cos002,cos109 not supported yet

            ///
            // Variants arrays for costumes with variants
            string[] cos001AllVariants = {"cos001_a","cos001_b","cos001_c","cos001_d"};
            string[] cos005AllVariants = {"cos005_a","cos005_b","cos005_c","cos005_d"};
            string[] cos006AllVariants = {"cos006_a","cos006_b"};
            ///
            foreach (var cos in costumeList)
            {
                var fi = Path.Combine(costumePaks, cos.FileName);

                var pakName = Path.GetFileName(fi);
                updateOutput($"Reading {pakName}");

                var newFolderPath = Path.Combine(Path.GetDirectoryName(fi), Path.GetFileNameWithoutExtension(fi));
                var newFilePath = Path.Combine(newFolderPath, pakName);
                Directory.CreateDirectory(newFolderPath);
                File.Copy(fi, newFilePath, true);

                PakUtils.ExtractAllFromPak(newFolderPath);
                updateOutput(fi);
                // Do something about checking if all variations are there or else default to the first one available
                    var meshMultiple = checkMutltipleMeshs(newFolderPath);
                     //updateOutput(meshMultiple.Count.ToString());         
                    //Skip check if costume only has one file and not matched the possible costumes with variant
                    if (meshMultiple.Count > 1 && CostumeIDsWithVariationNoLetter.Any(c => meshMultiple[0].Contains(c))) {
                        var meshMultipleConcat = string.Join(" ",meshMultiple);
                            //Check if the costumes are in the costumes with mesh variant folders
                            if (
                                    cos001AllVariants.All(c => meshMultipleConcat.Contains(c)) ||
                                    cos005AllVariants.All(c => meshMultipleConcat.Contains(c)) ||
                                    cos006AllVariants.All(c => meshMultipleConcat.Contains(c))
                                ) 
                                {
                                cos.eligibleForMeshVariant = true;
                                updateOutput("Costume is eligible for mesh variant");
                            }
                            //Else break this loop
                            else {
                                updateOutput("Found only some variants, defaulting to the first variant");
                            }
                        }
                    

                var Mesh = checkMesh(newFolderPath);
                if (Mesh != string.Empty)
                {
                    cos.BodyMesh = $"/Game/Model/Character/Body/Cos/chr_body_{cos.ID}/Mesh/{Mesh}.{Mesh}(0)";
                } else
                {
                    updateOutput("Mesh not found, skipping...");
                    Directory.Delete(newFolderPath, true);
                    continue;
                }

                var GPA = checkGPA(newFolderPath);
                if (GPA != string.Empty)
                {
                    updateOutput($"GPA file found...");
                    cos.BodyGmPhysicsAsset = $"/Game/Model/Character/Body/Cos/chr_body_{cos.ID}/Mesh/{GPA}.{GPA}(0)";
                }

                //Do something about checking if the costume has custom accessory/accessories
                var Accessories = checkAccessories(newFolderPath);
                if (Accessories.Count > 0) {
                    foreach (var acce in Accessories) {
                        var NewAcce = new AttachAccessory(acce);
                        NewAcce.MeshData = $"/Game/Model/Character/Body/Cos/chr_body_{cos.ID}/Mesh/{acce}.{acce}(0)";
                        cos.accessories.Add(NewAcce);
                    }
                }
                //This part checks if the icon for the costume exists, if so, applies the new iconID
                bool iconExists = false;
                var iconID = "WDT_" + cos.ID + "A";
                iconExists = copyCostumeIcon(extractedFolder,newFolderPath,iconID);
                if (iconExists) {
                    cos.IconID = iconID;
                }

                AssetUtils.ReplacePaths(newFolderPath, gamePaks, cos);
                AssetUtils.CopyAll(Path.Combine(newFolderPath, "extracted"), extractedFolder);

                // No idea why some files don't close properly (probably a CUE4Parse bug), but this works.
                try
                {
                    Directory.Delete(newFolderPath, true);
                }
                catch (System.IO.IOException)
                {
                    Program.strayFolders.Add(newFolderPath);
                }
            }

            AddAllEntries(costumeList, extractedFolder);
            //Copy Default Icon not the individual costume icon
            copyIcon(currentPath, extractedFolder);
           
            //Merge new AssetRegistry
            AssetRegistryHelper.MergeAssetRegistry
            (
                Path.Combine(extractedFolder,"StarlitSeason" ,"AssetRegistry.bin")
            );

            updateOutput("Packing mods...");
            var outputCostumesPak = Path.Combine(currentPath, "costumes.pak");
            var modsFolder = Path.Combine(gamePaks, @"~mods") ;

            UnrealPakUtils.RePackPak(outputCostumesPak, extractedFolder);
            Directory.Delete(extractedFolder, true);

            if (!Directory.Exists(modsFolder))
            {
                Directory.CreateDirectory(modsFolder);
            }

            File.Move(outputCostumesPak, Path.Combine(modsFolder, Path.GetFileName(outputCostumesPak)), true);
            File.Copy(Path.Combine(gamePaks, "pakchunk0-WindowsNoEditor.sig"), Path.Combine(modsFolder, Path.GetFileNameWithoutExtension(outputCostumesPak)+".sig"), true);

            updateOutput("Done.");
        }

        private static string checkGPA(string folder)
        {
            var result = Directory.GetFiles(folder, "GPA_chr_body_*.uasset", SearchOption.AllDirectories);

            if (result.Length > 0)
            {
                return Path.GetFileNameWithoutExtension(result[0]);
            }

            return string.Empty;
        }

        private static string checkMesh(string folder)
        {
            var result = Directory.GetFiles(folder, "SK_chr_body_*.uasset", SearchOption.AllDirectories);

            if (result.Length > 0)
            {
                return Path.GetFileNameWithoutExtension(result[0]);
            }

            return string.Empty;
        }

        private static List<String> checkMutltipleMeshs(string folder)
        {
            var resultList = new List<String>();
            var result = Directory.GetFiles(folder, "SK_chr_body_*.uasset", SearchOption.AllDirectories);

            if (result.Length > 0)
            {
                foreach (var res in result) {
                    resultList.Add( Path.GetFileNameWithoutExtension(res));
                }
                return resultList;
            }

            return resultList;
        }

        //Only works for accessories in the Cos folder atm
        private static List<String> checkAccessories(string folder)
        {
            var AccePath = Path.Combine(folder,"extracted","StarlitSeason","Content","Model","Character","Body","Cos");
            var resultList = new List<String>();
            var result = Directory.GetFiles
                         (AccePath, "SK_chr_acce_*.uasset", SearchOption.AllDirectories).Union(
                         Directory.GetFiles
                         (AccePath, "SK_chr_01_acce_*.uasset",SearchOption.AllDirectories)
                         ).ToArray();
            if (result.Length > 0)
            {
                foreach (var res in result) {
                    resultList.Add( Path.GetFileNameWithoutExtension(res));
                }
                return resultList;
            }

            return resultList;
        }

        private static JObject GetJSONCharacterDataTable(string dataTablePath, Costume cos) 
        {
            var CharacterCostume_DataTable = AssetUtils.ReadFileAsJson(dataTablePath+".uasset");
            foreach (var item in CharacterCostume_DataTable["Exports"][0]["Table"]["Data"])
            {
                var name = cos.BaseID.Contains("_") ? cos.BaseID.Split("_").First() : cos.BaseID;
                if (item["Name"].ToString() == name+"(0)")
                {
                    // updateOutput(cos.BaseID);
                    item["Name"] = cos.ID+"(0)";
                    item["Value"][1]["Value"] = cos.ID+"(0)";
                    return (JObject) item;
                }
            }

            updateOutput("WARNING: ID not found, using cos001A...");
            return (JObject) CharacterCostume_DataTable["Exports"][0]["Table"]["Data"][0];
        }

        //Does the same as above but for retrieve costume with off variation
        private static JObject GetJSONCharacterDataTableCostumeOff(string dataTablePath, Costume cos) 
        {
            var CharacterCostume_DataTable = AssetUtils.ReadFileAsJson(dataTablePath+".uasset");
            foreach (var item in CharacterCostume_DataTable["Exports"][0]["Table"]["Data"])
            {
                var name = cos.BaseID.Contains("_") ? cos.BaseID.Split("_").First() : cos.BaseID;
                if (item["Name"].ToString() == name+"_off(0)")
                {
                    // updateOutput(cos.BaseID);
                    item["Name"] = cos.ID+"(0)";
                    item["Value"][1]["Value"] = cos.ID+"(0)";
                    return (JObject) item;
                }
            }

            updateOutput("WARNING: ID not found, using cos001A...");
            return (JObject) CharacterCostume_DataTable["Exports"][0]["Table"]["Data"][0];
        }



        private static Tuple<JObject, string>  GetJSONCostumeModelDataTable(string dataTablePath, Costume cos)
        {
            var CostumeModelDataTable = AssetUtils.ReadFileAsJson(dataTablePath+".uasset");
            
            foreach (var item in CostumeModelDataTable["Exports"][0]["Table"]["Data"])
            {
                var tableMesh = item["Value"][0]["Value"].ToString().Split(".").Last().Replace("(0)", "").Replace("_inner", "");
                var cosMesh = cos.BodyMesh.Split(".").Last().Replace("(0)", "").Replace("_inner", "");
               
                if (tableMesh == cosMesh)
                {
                    var baseID = item["Name"].ToString().Replace("(0)", "");
                    item["Name"] = cos.ID+"(0)";
                    item["Value"][0]["Value"] = cos.BodyMesh;

                    if (cos.BodyGmPhysicsAsset != string.Empty)
                    {
                        item["Value"][1]["Value"] = cos.BodyGmPhysicsAsset;
                    }

                    return new Tuple<JObject, string>((JObject) item, baseID);
                }
            }

            updateOutput("WARNING: ID not found, using cos001A_a...");
            return new Tuple<JObject, string>((JObject) CostumeModelDataTable["Exports"][0]["Table"]["Data"][0], "cos001A_a");
        }

        private static List<JObject> GetJSONCostumeModelDataTableVariants(string dataTablePath, Costume cos) 
        {
            var CostumeModelDataTable = AssetUtils.ReadFileAsJson(dataTablePath+".uasset");
            List<JObject> costumeModelList = new List<JObject>();

            // Costumes with mesh variations (not sure if missing some)
            string[] CostumeIDsWithVariation = {"cos001A","cos005A","cos006A"}; // cos002 , cos109 not supported yet

            foreach (var item in CostumeModelDataTable["Exports"][0]["Table"]["Data"]) 
            {
                var tableMesh = item["Value"][0]["Value"].ToString().Split(".").Last().Replace("(0)", "");
                var cosMesh = cos.BodyMesh.Split(".").Last().Replace("(0)", "");
                
                //For costumes with mesh variations
                if (cos.eligibleForMeshVariant && CostumeIDsWithVariation.Any( c=>  item["Name"].ToString().Replace("(0)", "").Contains(c))) 
                {
                    var tableCostumeBaseIdName = item["Name"].ToString().Replace("(0)", "");
                    if (cos.BaseID.Contains(tableCostumeBaseIdName.Split("_").First())) {
                        //Might need to check mesh name too
                        //Might need to check for missing mesh file
                        //updateOutput(cos.BaseID);
                        //updateOutput(tableMesh);
                        //updateOutput(tableCostumeBaseIdName.Split("_").First());
                        item["Value"][0]["Value"] = tableMesh;
                        if (cos.BodyGmPhysicsAsset != string.Empty)
                        {
                            item["Value"][1]["Value"] = cos.BodyGmPhysicsAsset;
                        }
                        costumeModelList.Add((JObject) item);
                    }
                }
                
                //For costumes with only texture variations/no mesh variation
                else if (tableMesh == cosMesh) 
                {
                    item["Value"][0]["Value"] = cos.BodyMesh;
                    

                    if (cos.BodyGmPhysicsAsset != string.Empty)
                    {
                        item["Value"][1]["Value"] = cos.BodyGmPhysicsAsset;
                    }

                    costumeModelList.Add((JObject) item);
                }

                
            }

            return costumeModelList;
        }

        private static void copyIcon(string currentPath, string extractedFolder) 
        {
            List<string> fileList = new List<string>()
            {
                "WDT_DLC_SYS_000.uasset",
                "WDT_DLC_SYS_000.ubulk",
                "WDT_DLC_SYS_000.uexp"
            };

            var path1 = Path.Combine(currentPath, "tools");
            var path2 = Path.Combine(extractedFolder, "StarlitSeason", "Content", "Widget", "LoadTexture", "DLC");
            Directory.CreateDirectory(path2);

            foreach (var file in fileList)
            {
                File.Copy(Path.Combine(path1, file), Path.Combine(path2, file));
            }
        }

        //Search and copy costume icon, true if found and copied, false if fails        
        private static bool copyCostumeIcon(string extractedFolder, string newFolderPath, string iconID) {
            var modWidgetPath = Path.Combine(newFolderPath, "extracted", "StarlitSeason", "Content", "Widget", "LoadTexture", "UnitDress");
            //Proceed to find icon files if the icon folder exists
            if (Directory.Exists(modWidgetPath)) {
                    FileInfo[] iconFiles = new DirectoryInfo(modWidgetPath).GetFiles();            
                    string fileNameNoExtension = Path.GetFileNameWithoutExtension(iconFiles[0].Name);
                    foreach (var file in iconFiles) {
                        File.Move(file.FullName,file.FullName.Replace(fileNameNoExtension,iconID));
                    }
                    return true;    
            }
            return false;
        }


        private static void AddAllEntries(List<Costume> costumeList, string extractedFolder)
        {
            // MD_ItemCostume
            updateOutput("Adding entries to MD_ItemCostume...");
            var MD_ItemCostumePath = Path.Combine(extractedFolder, Path.Combine(PakFiles.MD_ItemCostumeFile.Split("/")));
            List<JObject> newMD_ItemCostumeItems = new List<JObject>();
            List<string> MD_ItemCostumeNames = new List<string>();
            foreach (var costume in costumeList)
            {
                var  newMD_ItemCostumeItem = new MD_ItemCostumeEntry(costume.ID, costume.SortNum, "WDT_DLC_SYS_000", costume.Name, costume.Description);
                //If the costume has an iconID, uses the iconID instead of the default icon
                if (!String.IsNullOrEmpty(costume.IconID)) {
                    newMD_ItemCostumeItem.IconID = costume.IconID;
                }
                newMD_ItemCostumeItems.Add(newMD_ItemCostumeItem.ToJson());
                MD_ItemCostumeNames.Add(newMD_ItemCostumeItem.Name);
            }

            AssetUtils.AddEntries(MD_ItemCostumePath, newMD_ItemCostumeItems, MD_ItemCostumeNames);

            // MD_DLCUnlockConditionSteam
            // updateOutput("Adding entries to MD_DLCUnlockConditionSteam...");
            var MD_DLCUnlockConditionSteamPath = Path.Combine(extractedFolder, Path.Combine(PakFiles.MD_DLCUnlockConditionSteamFile.Split("/")));
            List<JObject> newMD_DLCUnlockConditionSteamItems = new List<JObject>();
            List<string> MD_DLCUnlockConditionSteamNames = new List<string>();
            foreach (var costume in costumeList) 
            {
                var costumeID = costume.ID.Replace("(0)", "");
                var newMD_DLCUnlockConditionSteamItem = new MD_DLCUnlockConditionSteamEntry(costumeID);
                newMD_DLCUnlockConditionSteamItems.Add(newMD_DLCUnlockConditionSteamItem.ToJson());
                MD_DLCUnlockConditionSteamNames.Add(newMD_DLCUnlockConditionSteamItem.Name);
            }

            AssetUtils.AddEntries(MD_DLCUnlockConditionSteamPath, newMD_DLCUnlockConditionSteamItems, MD_DLCUnlockConditionSteamNames);

            // CostumeModelDataTable
            updateOutput("Adding entries to CostumeModelDataTable...");
            var CostumeModelDataTablePath = Path.Combine(extractedFolder, Path.Combine(PakFiles.CostumeModelDataTableFile.Split("/")));
            List<JObject> newCostumeModelDataTableItems = new List<JObject>();
            List<string> CostumeModelDataTableNames = new List<string>();
            List<Costume> removeItems = new List<Costume>();
            List<Costume> newCostumeList = new List<Costume>();
            // Costumes with mesh variations (not sure if missing some)
            string[] CostumeIDsWithVariation = {"cos001A","cos005A","cos006A"}; // cos002 , cos109 not supported yet

            foreach (var costume in costumeList)
            {
                var newCostumeModelDataTableItem = GetJSONCostumeModelDataTable(CostumeModelDataTablePath, costume);
                costume.BaseID = newCostumeModelDataTableItem.Item2;
                // (Costumes with "_chr" imply color variant)
                
                // Reminder to do this for cos107 since its naming is different

                if (costume.BaseID.Contains("_chr"))
                {
                    // TODO: change the paths in the replacement textures (ReplacementTexture1(0))
                    removeItems.Add(costume);
                    var costumeVariants = GetJSONCostumeModelDataTableVariants(CostumeModelDataTablePath, costume);
                    Regex oldCostumeIdTextureRegex = null;
                    foreach (var variant in costumeVariants)
                    {
                        var newCostume = new Costume("", costume.Name, costume.Description);
                        newCostume.BaseID = variant["Name"].ToString().Replace("(0)", "");

                        var namePrefix = costume.BaseID.Split('_').First();
                        var newName = newCostume.BaseID.Replace(namePrefix, costume.ID);
                        variant["Name"] = newName;
                        //Replacing the path for replacement textures
                        var oldTexturePath = variant["Value"][3]["Value"][0]["Value"].ToString().Replace("(0)", "");
                            if (oldCostumeIdTextureRegex == null) {
                                oldCostumeIdTextureRegex = new Regex(namePrefix.Remove(namePrefix.Length-1));
                        }
                        var newTexturePath = oldCostumeIdTextureRegex.Replace(oldTexturePath,newName.Split('_').First(),1);
                        variant["Value"][3]["Value"][0]["Value"] = newTexturePath;
                        
                        newCostume.ID = newName;
                        newCostume.BodyMesh = variant["Value"][0]["Value"].ToString().Replace("(0)", "");

                        newCostumeList.Add(newCostume);

                        newCostumeModelDataTableItems.Add(variant);

                        CostumeModelDataTableNames.Add(newCostume.ID);
                        CostumeModelDataTableNames.Add(newCostume.BodyMesh);
                        CostumeModelDataTableNames.Add(newTexturePath);
                    }
                } 
                // Costumes with mesh variation
                else if (costume.eligibleForMeshVariant &&  CostumeIDsWithVariation.Any( c=> costume.BaseID.Contains(c))) 
                {
                    var costumeVariants = GetJSONCostumeModelDataTableVariants(CostumeModelDataTablePath, costume);
                   
                    foreach (var variant in costumeVariants)
                    {
                        //Original Prefix Name
                        var namePrefix = costume.BaseID.Split('_').First();
                        //Generate New Name
                        var newName = variant["Name"].ToString().Replace("(0)", "").Replace(namePrefix, costume.ID);
                        var originalBodyMesh = costume.BodyMesh.Split(".").Last().Replace("(0)", "");
                        var NewBodyMesh = costume.BodyMesh.Replace( originalBodyMesh , variant["Value"][0]["Value"].ToString().Replace("(0)", "")).Replace("(0)", ""); 

                        variant["Name"] = newName;
                        variant["Value"][0]["Value"] = NewBodyMesh;

                        newCostumeModelDataTableItems.Add(variant);

                        CostumeModelDataTableNames.Add(costume.ID);
                        CostumeModelDataTableNames.Add(newName);
                        CostumeModelDataTableNames.Add(NewBodyMesh);                      
                       
                    }
                 
                }
                              
                // Normal costumes
                else   
                {
                    newCostumeModelDataTableItems.Add(newCostumeModelDataTableItem.Item1);
                    CostumeModelDataTableNames.Add(costume.ID);
                    CostumeModelDataTableNames.Add(costume.BodyMesh);
                }

                if (costume.BodyGmPhysicsAsset != string.Empty) 
                {
                    CostumeModelDataTableNames.Add(costume.BodyGmPhysicsAsset);
                }

                // Workaround because cos008A and cos008B are the only costumes that use the WearMesh parameter
                if (costume.BaseID.Contains("cos008"))
                {
                    var wearMesh = $"/Game/Model/Character/Body/Cos/chr_body_{costume.ID}/Mesh/SK_chr_body_cos008_a.SK_chr_body_cos008_a";
                    var Mesh = $"/Game/Model/Character/Body/Cos/chr_body_{costume.ID}/Mesh/SK_chr_body_cos008_a_inner.SK_chr_body_cos008_a_inner";
                    newCostumeModelDataTableItem.Item1["Value"][2]["Value"] = wearMesh;
                    newCostumeModelDataTableItem.Item1["Value"][0]["Value"] = Mesh;
                    CostumeModelDataTableNames.Add(wearMesh);
                    CostumeModelDataTableNames.Add(Mesh);
                }

                // Workaround because UAssetAPI breaks WidthMode
                //Apparently WidthMode is broken on all costumes actually
                if (costume.BaseID == "cos104A")
                {
                    var wfix = JObject.Parse(jsonTemplates.WidthModeFix);
                    newCostumeModelDataTableItem.Item1["Value"][7]["Value"][15] = wfix;
                }
            }

            AssetUtils.AddEntries(CostumeModelDataTablePath, newCostumeModelDataTableItems, CostumeModelDataTableNames);

            // MD_CostumeModel
            updateOutput("Adding entries to MD_CostumeModel...");
            var MD_CostumeModelPath = Path.Combine(extractedFolder, Path.Combine(PakFiles.MD_CostumeModelFile.Split("/")));
            List<JObject> newMD_CostumeModelItems = new List<JObject>();
            List<string> MD_CostumeModelNames = new List<string>();
            foreach (var costume in costumeList)
            {
                var baseID = costume.BaseID.Contains('_') ? costume.BaseID.Split('_').First() : costume.BaseID;
                var newMD_CostumeModelItem = new MD_CostumeModelEntry(costume.ID, baseID+"_off(0)");
                //Make an addition _off variation if the costume has accessories and has toggleable accessories
                if (newMD_CostumeModelItem.CostumeID2.Contains("_off") && costume.accessories.Count > 0) {
                    newMD_CostumeModelItem.CostumeID2 = costume.ID+ "_off(0)";
                    costume.ToggleAbleAccessories = true;
                    MD_CostumeModelNames.Add(newMD_CostumeModelItem.CostumeID2);
                }
                newMD_CostumeModelItems.Add(newMD_CostumeModelItem.ToJson());
                MD_CostumeModelNames.Add(newMD_CostumeModelItem.Name);
            }
            
            AssetUtils.AddEntries(MD_CostumeModelPath, newMD_CostumeModelItems, MD_CostumeModelNames);

            foreach (var item in removeItems)
            {
                costumeList.Remove(item);
            }
            costumeList.AddRange(newCostumeList);

            // AttachAccessoryModelDataTable
            updateOutput("Adding entries to AttachAccessoryModelDataTable...");
            var AttachAccessoryModelDataTablePath = Path.Combine(extractedFolder, Path.Combine(PakFiles.AttachAccessoryModelDataTableFile.Split("/")));
            List<JObject> NewAttachAccessoryModelDataTable = new List<JObject>();
            List<string> AttachAccessoryModelDataTableName = new List<string>();
            
            //Loop through each costume to find accessories
            foreach (var costume in costumeList) 
            {
                //If the costume is packed with custom accessories
                if(costume.accessories.Count > 0) {
                    foreach(var acce in costume.accessories) {
                        var AttachAccessoryModelDataTable = AssetUtils.ReadFileAsJson(AttachAccessoryModelDataTablePath+".uasset");
                        foreach (var item in AttachAccessoryModelDataTable["Exports"][0]["Table"]["Data"]) {

                            var OGMeshData = item["Value"][0]["Value"].ToString().Split('.').Last().Replace("(0)", "");
                            if (OGMeshData == acce.FileName) {
                                acce.originalAccessoryName = item["Name"].ToString().Replace("(0)", "");
                                var cosID = costume.BaseID.Remove(costume.BaseID.Length-1);
                                item["Name"] = acce.originalAccessoryName.Replace(cosID,costume.ID);
                                item["Value"][0]["Value"] = acce.MeshData;
                                acce.ID = item["Name"].ToString();
                                NewAttachAccessoryModelDataTable.Add((JObject)item);
                                AttachAccessoryModelDataTableName.Add(OGMeshData);
                                AttachAccessoryModelDataTableName.Add(acce.MeshData);
                                AttachAccessoryModelDataTableName.Add(item["Name"].ToString().Replace("(0)", ""));
                                break;
                            }                        
                        }
                    }
                }
            }
            AssetUtils.AddEntries(AttachAccessoryModelDataTablePath, NewAttachAccessoryModelDataTable,  AttachAccessoryModelDataTableName);

            // CharacterCostume_chrxxx_DataTable
            foreach (var file in PakFiles.CharacterCostumeFolder)
            {
                var tableName = Path.GetFileNameWithoutExtension(file);
                updateOutput($"Adding entries to {tableName}...");

                var CharacterCostume_DataTablePath = Path.Combine(extractedFolder, Path.Combine(file.Split("/")));

                if (!File.Exists(CharacterCostume_DataTablePath+".uasset"))
                {
                    continue;
                }

                List<JObject> newCharacterCostume_DataTableItems = new List<JObject>();
                List<string> CharacterCostume_DataTableNames = new List<string>();


                foreach (var costume in costumeList)
                {
                    // (Costumes with "_chr" imply color variant)
                    if (costume.BaseID.Contains("_chr"))
                    {
                        var idolIDCos = costume.BaseID.Split('_').Last().Replace("(0)", "");
                        var idolIDTab = tableName.Split('_')[1];
                        if (idolIDCos == idolIDTab)
                        {   
                            var newName = costume.ID.Split('_').First().Replace("(0)", "");
                            var newCharacterCostume_DataTableItem = GetJSONCharacterDataTable(CharacterCostume_DataTablePath, costume);
                            newCharacterCostume_DataTableItem["Value"][1]["Value"] = costume.ID;
                            newCharacterCostume_DataTableItem["Name"] = newName;

                            //This part is supposed to be for adding Accessories
                            addCustomAccessoriesCharacterCostume(newCharacterCostume_DataTableItem,costume,CharacterCostume_DataTableNames);
                            // If costume has toggleable accessories, create additional off variation
                                if (costume.ToggleAbleAccessories) {
                                    var newCharacterCostumeOff_DataTableItem = 
                                        createOffVariationCharacterCostume
                                        (newCharacterCostume_DataTableItem,costume,CharacterCostume_DataTableNames,CharacterCostume_DataTablePath);
                                    newCharacterCostume_DataTableItems.Add(newCharacterCostumeOff_DataTableItem);
                                }
                            //////////
                                                      
                            newCharacterCostume_DataTableItems.Add(newCharacterCostume_DataTableItem);
                            CharacterCostume_DataTableNames.Add(costume.ID);
                            CharacterCostume_DataTableNames.Add(newName);
                        }
                    }
                    // Costumes with mesh variation
                    else if (costume.eligibleForMeshVariant && CostumeIDsWithVariation.Any( c=> costume.BaseID.Contains(c))) 
                    {
                            var newName = costume.ID.Split('_').First().Replace("(0)", "");
                            var newCharacterCostume_DataTableItem = GetJSONCharacterDataTable(CharacterCostume_DataTablePath, costume);
                            
                            var CostumeModelDataTable = AssetUtils.ReadFileAsJson(CharacterCostume_DataTablePath+".uasset");
                            //This loop is ineffecicient but idk what else to do
                            foreach (var item in CostumeModelDataTable["Exports"][0]["Table"]["Data"])
                            {                   
                                var realBaseID = item["Name"].ToString().Replace("(0)", "").Split('_').First();
                                var baseIDSplit = costume.BaseID.Split('_').First();
                            // If costume ID matches
                            if (realBaseID  == baseIDSplit) {
                                var variant = item["Value"][1]["Value"].ToString().Split("_").Last().Replace("(0)","");
                                newCharacterCostume_DataTableItem["Value"][1]["Value"] = newName + "_" + variant;
                                newCharacterCostume_DataTableItem["Name"] = newName;

                                //This part is supposed to be for adding Accessories
                                addCustomAccessoriesCharacterCostume(newCharacterCostume_DataTableItem,costume,CharacterCostume_DataTableNames);
                                // If costume has toggleable accessories, create additional off variation
                                    if (costume.ToggleAbleAccessories) {
                                        var newCharacterCostumeOff_DataTableItem = 
                                            createOffVariationCharacterCostume
                                            (newCharacterCostume_DataTableItem,costume,CharacterCostume_DataTableNames,CharacterCostume_DataTablePath);
                                        newCharacterCostume_DataTableItems.Add(newCharacterCostumeOff_DataTableItem);
                                    }
                                //////////             
                                newCharacterCostume_DataTableItems.Add(newCharacterCostume_DataTableItem);
                                CharacterCostume_DataTableNames.Add(costume.ID);
                                CharacterCostume_DataTableNames.Add(newName);
                                CharacterCostume_DataTableNames.Add(newName + "_" + variant);
                                break;
                            }
                            }                      
                    }
                    // Normal costumes
                     else
                    {
                        var newCharacterCostume_DataTableItem = GetJSONCharacterDataTable(CharacterCostume_DataTablePath, costume);
                        //This part is supposed to be for adding Accessories
                        addCustomAccessoriesCharacterCostume(newCharacterCostume_DataTableItem,costume,CharacterCostume_DataTableNames);
                        // If costume has toggleable accessories, create additional off variation
                            if (costume.ToggleAbleAccessories) {
                                var newCharacterCostumeOff_DataTableItem = 
                                    createOffVariationCharacterCostume
                                    (newCharacterCostume_DataTableItem,costume,CharacterCostume_DataTableNames,CharacterCostume_DataTablePath);
                                 newCharacterCostume_DataTableItems.Add(newCharacterCostumeOff_DataTableItem);
                            }
                        //////////
                        newCharacterCostume_DataTableItems.Add(newCharacterCostume_DataTableItem);
                        CharacterCostume_DataTableNames.Add(costume.ID);
                    }
                }

                AssetUtils.AddEntries(CharacterCostume_DataTablePath, newCharacterCostume_DataTableItems, CharacterCostume_DataTableNames);
            }
        }

        //Add reference of the new custom accessories to the character costume
        private static void addCustomAccessoriesCharacterCostume(JObject CharacterCostume_DataTableItem,Costume costume, List<String> CharacterCostume_DataTableNames) {
            //The indice for the accessories are fixed 
            int[] AccessoryIndiceArr = {10,15,20,25,30};
            foreach (var acce in costume.accessories) {    
            for (var i = 0; i < AccessoryIndiceArr.Length; i++) { 
                var OGAccessoryName = CharacterCostume_DataTableItem["Value"][AccessoryIndiceArr[i]]["Value"].ToString().Replace("(0)","");
                        if (OGAccessoryName == acce.originalAccessoryName) {
                                CharacterCostume_DataTableItem["Value"][AccessoryIndiceArr[i]]["Value"] = acce.ID;
                                CharacterCostume_DataTableNames.Add(acce.ID);
                                break;
                    }
                }
            }
        }
        //Create and apply _off variation of the costume with accessories
        private static JObject createOffVariationCharacterCostume
        (JObject CharacterCostume_DataTableItem,Costume costume, List<String> CharacterCostume_DataTableNames,
         String CharacterCostume_DataTablePath) {
             var newCharacterCostumeOff_DataTableItemReference = GetJSONCharacterDataTableCostumeOff(CharacterCostume_DataTablePath, costume);
             var newCharacterCostumeOff_DataTableItem =
            (JObject) JsonConvert.DeserializeObject(JsonConvert.SerializeObject(CharacterCostume_DataTableItem));
                newCharacterCostumeOff_DataTableItem["Name"] =  CharacterCostume_DataTableItem["Name"].ToString().Replace("(0)", "") + "_off";
                CharacterCostume_DataTableNames.Add(newCharacterCostumeOff_DataTableItem["Name"].ToString());
                foreach (var acce in costume.accessories) {         
                    //The indice for the accessories are fixed 
                    int[] AcceIndiceArr = {10,15,20,25,30};
                    for (var i = 0; i < AcceIndiceArr.Length; i++) { 
                        var OGAccessoryName = newCharacterCostumeOff_DataTableItemReference["Value"][AcceIndiceArr[i]]["Value"].ToString().Replace("(0)",""); 
                            if (OGAccessoryName != acce.originalAccessoryName) {
                                newCharacterCostumeOff_DataTableItem["Value"][AcceIndiceArr[i]]["Value"] 
                                    = newCharacterCostumeOff_DataTableItemReference["Value"][AcceIndiceArr[i]]["Value"] ;

                                        }
                                    }
                                }
        return newCharacterCostumeOff_DataTableItem;
        }

    }
}
