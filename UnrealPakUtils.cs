using System;
using System.Diagnostics;
using System.IO;

namespace CustomSlots
{
    static class UnrealPakUtils
    {
        private static string GetCurrentDirectory() {
            return AppContext.BaseDirectory;
        }

        private static void RunUnrealPak(string cmdArgs)
        {
            var currentDirectory = GetCurrentDirectory();
            // This is because unrealpak creates some data folders 3 directories behind
            var exePath = Path.Combine(currentDirectory, "tools", "1", "2", "3", "UnrealPak.exe");
            var execName = "";
            
            if (System.OperatingSystem.IsLinux())
            {
                execName = "wine";
                cmdArgs = $"{exePath} {cmdArgs}";
            } else
            {
                execName = exePath;
            }

            using (Process unrealPackProcess = new Process())
                {
                    unrealPackProcess.StartInfo.UseShellExecute = false;
                    unrealPackProcess.StartInfo.FileName = execName;
                    unrealPackProcess.StartInfo.Arguments = cmdArgs;
                    unrealPackProcess.StartInfo.CreateNoWindow = true;
                    unrealPackProcess.StartInfo.RedirectStandardOutput = true;
                    unrealPackProcess.Start();

                    CustomSlots.updateOutput(unrealPackProcess.StandardOutput.ReadToEnd());
                    unrealPackProcess.WaitForExit();
                }
        }

        private static string CreateFileList(string dirPath)
        {
            var filePath = Path.Combine(GetCurrentDirectory(), "filelist.txt");
            using (StreamWriter file = new(filePath))
            {
                file.WriteLine($"\"{dirPath}\\*.*\" \"..\\..\\..\\*.*\" ");
            }
            
            return filePath;
        }

        public static void RePackPak(string pakPath, string dirPath)
        {
            string fileList = CreateFileList(dirPath);
            // var fileList = Path.Combine(GetCurrentDirectory(), "filelist.txt");
            var cmdArgs = $"\"{pakPath}\" -Create=\"{fileList}\" -compress";

            RunUnrealPak(cmdArgs);
        }
    }
}