namespace CustomSlots
{
    public class AttachAccessory
    {
        //Literal file name
        public string FileName;
        //Probably not needed
        public string ID = string.Empty;
        //Probably not needed
        public string BaseName;

        public string MeshData = string.Empty;
        public string GmPhysicsAsset = string.Empty;
        
        public string originalAccessoryName = string.Empty;
        public AttachAccessory(string fileName)
        {
            FileName = fileName;
            // ID = iD;
        }
    }
}
