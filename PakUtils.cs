using System.Collections.Generic;
using System.IO;

using CUE4Parse.Encryption.Aes;
using CUE4Parse.FileProvider;
using CUE4Parse.UE4.Objects.Core.Misc;

namespace CustomSlots
{
    static class PakUtils
    {
        private const string aesKey = "0x75D37B405AD9745CCB5BE53FD163326B6044D49B1134A9510835BBC8B46B2DDA";
        private static DefaultFileProvider InitProvider(string gameDirectory, bool isEncrypted = true) 
        {
            var provider = new DefaultFileProvider(gameDirectory, SearchOption.AllDirectories, false);
            provider.Initialize();

            if (isEncrypted)
            {
                provider.SubmitKey(new FGuid(), new FAesKey(aesKey));
            }

            return provider;
        }

        public static void ExtractFromPak(string gameDirectory, string objectPath, string outputPath)
        {
            var provider = InitProvider(gameDirectory);
            Directory.CreateDirectory(outputPath);

            if (provider.TrySavePackage(objectPath, out var assets))
            {
                foreach (var kvp in assets)
                {
                    var filePath = Path.Combine(outputPath, kvp.Key);
                    Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                    File.WriteAllBytes(filePath, kvp.Value);
                }
            }
            //Extract AssetRegistry
                else if (objectPath.Contains("/StarlitSeason/AssetRegistry")) {
                    provider.TrySaveAsset(objectPath +".bin", out var data);
                    var filePath = Path.Combine(outputPath,"StarlitSeason", "AssetRegistry.bin");
                    Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                    File.WriteAllBytes(filePath, data);
                }
            
        }

        public static void ExtractFromPak(string gameDirectory, string objectPath)
        {   
            var outputPath = Path.Combine(gameDirectory, "extracted");
            ExtractFromPak(gameDirectory, objectPath, outputPath);
        }

        public static void ExtractFromPak(string gameDirectory, List<string> objectPaths, string outputPath) 
        {   
            var provider = InitProvider(gameDirectory);
            // Directory.CreateDirectory(outputPath);

            foreach (var file in objectPaths)
            {
                if (provider.TrySavePackage(file, out var assets))
                {
                    foreach (var kvp in assets)
                    {
                        var filePath = Path.Combine(outputPath, kvp.Key);
                        Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                        File.WriteAllBytes(filePath, kvp.Value);
                        
                    }
                }
                //Extract AssetRegistry
                else if (file.Contains("/StarlitSeason/AssetRegistry")) {
                    provider.TrySaveAsset(file +".bin", out var data);
                    var filePath = Path.Combine(outputPath,"StarlitSeason", "AssetRegistry.bin");
                    Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                    File.WriteAllBytes(filePath, data);
                }
                }
                    
                }
            
        
        public static void ExtractAllFromPak(string gameDirectory, bool isEncrypted = true) 
        {   
            var provider = InitProvider(gameDirectory);

            foreach (var item in provider.Files.Keys)
            {
                var directory = Path.Combine(gameDirectory, "extracted");
                Directory.CreateDirectory(directory);

                if (provider.TrySavePackage(item, out var assets))
                {
                    foreach (var kvp in assets)
                    {
                        // Icons don't work so don't extract them.
                        //if (!kvp.Key.Contains("Widget/"))
                        {
                            var filePath = Path.Combine(directory, kvp.Key);
                            Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                            File.WriteAllBytes(filePath, kvp.Value);
                        }
                    }
                }
            }  
        }
    }
}