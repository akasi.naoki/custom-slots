# Custom Slots

Tool to add costumes to the datatables in THE iDOLM@STER Starlit Season.

For additional info and download links of the original tool see: https://gamebanana.com/mods/347715
###   New features
- Support costumes with color variations **(Tested on Cos007)**
    - **DISCLAIMER** : Not guaranteed to work with cos107 and cos109, testing is needed.
- Support costumes with mesh variations **(Currently only on Cos001, Cos005 and Cos006)**
    - The costume will default to the first model the game found if the costume pak is missing some files
        - EX: if The game only found A,B,D variation of Cos001 in the pak, it will default to A for all the variations.
        - EX: if The game only found C variation of Cos001 in the pak, it will default to C for all the variations.
- Support costumes with custom icons
    - Costumes that come with a custom icon will use the custom packed icon
    - Costumes that do not come with a custom icon will use the default MOD icon
- Support costumes with custom accessories packed within the paks
    - Costumes that come with custom accessories will use its own accessories instead of the original accessories the costume was based on
    - Costumes that comes with custom accessories and come with on/off variation with be given additional off variation with proper accessories.

## Requirements

- On Linux requires `dotnet-runtime`(6.0), `gtk-sharp-3` and `wine` (for unrealpak and AssetRegistryHelper).
- On Windows 7/8.1 requires [Microsoft Visual C++ 2015-2019 Redistributable](https://docs.microsoft.com/en-us/dotnet/core/install/windows?tabs=net60#additional-deps).

## Acknowledgments
- ToastyBuns for figuring out the datatables.
- https://github.com/atenfyr/UAssetAPI
- https://github.com/FabianFG/CUE4Parse
- Archengius for **Asset Registry Helper** for Dead by Daylight to help with merging Asset Registry slots.