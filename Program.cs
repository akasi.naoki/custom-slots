using System;
using System.IO;
using System.Collections.Generic;

using Gtk;

namespace CustomSlots
{
    static class Program
    {
        public static MainWindow win { get; set; }
        public static List<string> strayFolders { get; set; }
        [STAThread]
        public static void Main(string[] args)
        {
            strayFolders = new List<string>();
            if (System.OperatingSystem.IsWindows()) 
            {
                string systemPath = Environment.GetEnvironmentVariable("Path", EnvironmentVariableTarget.Machine);
                Environment.SetEnvironmentVariable("Path", $"{Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin")};{systemPath}");
            }
            
            Application.Init();

            var app = new Application("org.CustomSlots.CustomSlots", GLib.ApplicationFlags.None);
            app.Register(GLib.Cancellable.Current);

            win = new MainWindow();
            app.AddWindow(win);

            win.Show();
            Application.Run();
        }
    }
}
