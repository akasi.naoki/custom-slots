using System;
using System.Diagnostics;
using System.IO;

namespace CustomSlots
{
    static class AssetRegistryHelper
    {
        private static string GetCurrentDirectory() {
            return AppContext.BaseDirectory;
        }

        private static void RunAssetRegistryHelper(string cmdArgs)
        {
            var currentDirectory = GetCurrentDirectory();
            var exePath = Path.Combine(currentDirectory, "tools", "AssetRegistryHelper", "AssetRegistryHelper.exe");
            var execName = "";
            
            if (System.OperatingSystem.IsLinux())
            {
                execName = "wine";
                cmdArgs = $"{exePath} {cmdArgs}";
            } else
            {
                execName = exePath;
            }

            using (Process unrealPackProcess = new Process())
                {
                    unrealPackProcess.StartInfo.UseShellExecute = false;
                    unrealPackProcess.StartInfo.FileName = execName;
                    unrealPackProcess.StartInfo.Arguments = cmdArgs;
                    unrealPackProcess.StartInfo.CreateNoWindow = true;
                    unrealPackProcess.StartInfo.RedirectStandardOutput = true;
                    unrealPackProcess.Start();

                    CustomSlots.updateOutput(unrealPackProcess.StandardOutput.ReadToEnd());
                    unrealPackProcess.WaitForExit();
                }
        }

        public static void MergeAssetRegistry(string assetRegistryPath)
        {
            var modAssetRegistryPath = Path.Combine(GetCurrentDirectory(),"tools","AssetRegistryHelper","AssetRegistry.bin");
            var widgetFilterPath = Path.Combine("\\Game","Widget","LoadTexture","UnitDress").Replace("\\","/");
            var cmdArgs = $"\"{assetRegistryPath}\" -Merge \"{modAssetRegistryPath}\" \"{assetRegistryPath}\" -Filter=\"{widgetFilterPath}\"";

            RunAssetRegistryHelper(cmdArgs);
        }



    }

}